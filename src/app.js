'use strict';

// requirements
const express = require('express');
const { Amount } = require('./amount');
// constants
const PORT = process.env.PORT || 8080;

// main express program
const app = express();

// configurations
app.use(express.json());

// routes
// health check
app.get('/status', (req, res) => { res.status(200).end(); });
app.head('/status', (req, res) => { res.status(200).end(); });

// Send 
app.get('/', (req, res) => { 
    var balance = 1000;
    try {
        var { balance, transfered } = transfer(balance, req.query.amount);
    }
    catch {
        res.status(400).send('You can only transfer an amount');
    }
    if (balance !== undefined || transfered !== undefined) { 
        res.status(200).end('Successfully transfered: ' + transfered + '. Your balance: ' + balance);
    } else {
        res.status(400).end('Insufficient funds. Your balance: ' + balance);
    }
});

// Transfer amount service
var transfer = (balance, amount) => {
    const internal_amount = new Amount(amount);
    var transfered = 0;
    if (internal_amount.amount <= balance && internal_amount.amount > 0) {
        balance = balance - internal_amount.amount;
        transfered = transfered + internal_amount.amount;
        return { balance, transfered };
    } else
        return { undefined, undefined };
};

// Fix to avoid EADDRINUSE during test
if (!module.parent) {
    // HTTP listener
    app.listen(PORT, err => {
        if (err) {
            console.log(err);
            process.exit(1);
        }
        console.log('Server is listening on port: '.concat(PORT));
    });
}
// CTRL+c to come to action
process.on('SIGINT', function() {
    process.exit();
});

module.exports = { app, transfer };
