const isValidAmount = require('./validate');

class Amount {
    constructor(value) {
        if (!value || (value.length && value.length > 10)) {
            throw new RangeError("invalid input")
        }
        const num = Number(value);

        if (isValidAmount(value)) {
            throw new RangeError("invalid input");
        }
        this.amount = num;
    }
}
module.exports = {Amount}